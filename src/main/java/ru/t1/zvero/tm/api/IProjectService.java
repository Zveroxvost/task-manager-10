package ru.t1.zvero.tm.api;

import ru.t1.zvero.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    void clear();
}