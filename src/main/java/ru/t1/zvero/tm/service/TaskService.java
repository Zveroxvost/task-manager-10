package ru.t1.zvero.tm.service;

import ru.t1.zvero.tm.api.ITaskRepository;
import ru.t1.zvero.tm.api.ITaskService;
import ru.t1.zvero.tm.model.Task;
import ru.t1.zvero.tm.repository.TaskRepository;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setName(description);
        add(task);
        return task;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        taskRepository.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }
}