package ru.t1.zvero.tm.api;

import ru.t1.zvero.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}