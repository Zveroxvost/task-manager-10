package ru.t1.zvero.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}