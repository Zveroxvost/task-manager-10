package ru.t1.zvero.tm.api;

import ru.t1.zvero.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    void clear();

}