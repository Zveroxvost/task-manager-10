package ru.t1.zvero.tm.service;

import ru.t1.zvero.tm.api.IProjectRepository;
import ru.t1.zvero.tm.api.IProjectService;
import ru.t1.zvero.tm.model.Project;
import ru.t1.zvero.tm.repository.ProjectRepository;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        projectRepository.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }
}