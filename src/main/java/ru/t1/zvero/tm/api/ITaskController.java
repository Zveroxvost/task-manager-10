package ru.t1.zvero.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}